import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseServiceService} from './base-service.service';

@Injectable()
export class UtentiService extends BaseServiceService {


  constructor(private http: HttpClient) {
    super();
  }

  public listaUtenti() {
    const url = this.ApiUrl('https://jsonplaceholder.typicode.com/posts');
    return this.http.get(url);
  }
}
