import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { switchMap, concatMap, filter } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-my-expansion-panel',
  templateUrl: './my-expansion-panel.component.html',
  styleUrls: ['./my-expansion-panel.component.css'],
  animations: [
    trigger('apri', [ // il trigger è un contenitore di animazioni. apri nome del trigger
      state('state1', style({
        height: '0',
      })),
      state('state2', style({
        height: '*',
      })),
      transition('state1<=>state2', [
        animate('1000ms')
      ]),
    ]),
  ]
})
export class MyExpansionPanelComponent implements OnInit {
  title = 'ProgettoProgetto';
  commenti: string[];

  constructor(private http: HttpClient) {
    this.commenti = new Array();
  }

  ngOnInit() {
  }

  current = 'state1';

  Scorri() {
    this.current = 'state2'; // current=condizione?a:b;

    this.http.get('https://jsonplaceholder.typicode.com/posts') // prima chiamata mi tiro giu i posts
      .pipe(
        switchMap((posts: any) => from(posts)), // dalla lista dei post mi da il singolo post
        filter((singlePost: any) => (singlePost.id % 2 == 0)), // filtra il singolo post che arrivano dal from e da solo i risultati pari
        concatMap((evenPost: any) => this.http.get
          ('https://jsonplaceholder.typicode.com/comments?postId=' + evenPost.id)), // seconda chiamata
        switchMap((comments: any) => of(comments[comments.length - 1]))
      )
      .subscribe((lastComments: any) => this.commenti.push(lastComments.body)) // di ogni elemento che è entrato fa il console.log.body
    //console.log(lastComments.body)
  }
  Rimuovi(){
    this.current = 'state1';
    this.commenti=[];
  }

}


