import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyExpansionPanelComponent } from './my-expansion-panel.component';

describe('MyExpansionPanelComponent', () => {
  let component: MyExpansionPanelComponent;
  let fixture: ComponentFixture<MyExpansionPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyExpansionPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyExpansionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
