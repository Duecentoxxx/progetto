import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UtentiService } from 'src/_models/utenti-service.service';
import { HttpClientModule } from '@angular/common/http';
import { ListaComponent } from './lista/lista.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MyExpansionPanelComponent } from './my-expansion-panel/my-expansion-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    MyExpansionPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
