import { Component } from '@angular/core';
import { from, of } from 'rxjs';
import { filter, switchMap, concatMap} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { format } from 'url';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProgettoProgetto';
  //commenti:string[];

  //constructor(private http: HttpClient){
   // this.commenti = new Array();
  //}

  //ngOnInit():void {
    // this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe(data =>{
    //   console.log(data);
    // }) 
    //this.getPostTitlesAndBodyOfEventIdPost() // per richiamare una funzione basta scrivere il nome della funzione con ()
    //this.getLastCommentsBodyOfPostId()
  }
/*
  getPostTitlesAndBodyOfEventIdPost() { // prende solo quelli pari
    this.http.get('https://jsonplaceholder.typicode.com/posts')
    .pipe(
      switchMap((post:any) => from(post)),
      filter((post:any) => (post.id % 2 == 0))
    )
    .subscribe((post:any) => {
      console.log(post.title)
      console.log(post.body)
    })
  }
*/
/*  
getCommentsBodyOfPostId(){
    this.http.get('https://jsonplaceholder.typicode.com/comments')
    .pipe(
      switchMap((comments:any) => from(comments)),
      filter((comments:any) => (comments.postId % 2 == 0)),
      filter((comments:any) => (comments.id % 10 == 0))
    )
    .subscribe((post:any) => {
      console.log(post.id+' '+post.postId+' '+post.body)
    })
  }
  */
 
/*
 getLastCommentsBodyOfPostId(){
  this.http.get('https://jsonplaceholder.typicode.com/posts') // prima chiamata mi tiro giu i posts
  .pipe(
    switchMap((posts:any) => from(posts)), // dalla lista dei post mi da il singolo post
    filter((singlePost:any) => (singlePost.id % 2 == 0)), // filtra il singolo post che arrivano dal from e da solo i risultati pari
    concatMap((evenPost:any) => this.http.get
    ('https://jsonplaceholder.typicode.com/comments?postId=' + evenPost.id)), // seconda chiamata
    switchMap((comments:any) => of(comments [comments.length-1]))
  )
  .subscribe((lastComments:any) => console.log(lastComments.body)) // di ogni elemento che è entrato fa il console.log.body
  }*/
//}


